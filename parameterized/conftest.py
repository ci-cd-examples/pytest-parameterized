# Create a series of tests
def create_tests():
    testItems = []
    for i in range(0, 10):
        testItems.append(["ParameterizedTest" + str(i), True])

    return testItems

# Perform test parameterization
def pytest_generate_tests(metafunc):
    # Parameterize the tests. This would be the same thing as doing:
    # @pytest.mark.parametrize('tmp_ct', range(int(metafunc.config.option.tests)))
    testItems = create_tests()
    metafunc.parametrize(argnames='testItem', argvalues=[vector[1] for vector in testItems], ids=[vector[0] for vector in testItems])