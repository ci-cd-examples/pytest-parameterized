# pytest-parameterized

Parameterized test examples using the [Pytest framework](https://docs.pytest.org/en/stable/).

## Contents
The following is a description of the contents of this repository

### individual/
The "individual" folder contains tests that run as separately defined functions

### parameterized/
The "parameterized" folder contains tests that run in a parameterized fashion. This means a single function has been created but the many tests can be run.

## How to run
First install the required Python libraries
```
python -m pip install -r requirements.txt
```

Run the tests with the following (example) command:
```
python -m pytest -s individual/basic-example.py parameterized/param-example.py
```